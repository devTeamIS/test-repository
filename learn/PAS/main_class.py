from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QDialog, QMainWindow, QWidget, QPushButton
import PyQt5.uic as uic
import sys
# from ui_sign import Ui_MainWindow
# from ui import Ui_Dialog
import sqlite3


formMain = uic.loadUiType("2.ui")[0]
formDialog = uic.loadUiType("1.ui")[0]
formError = uic.loadUiType("Error.ui")[0]
formTableOrders = uic.loadUiType("Orders.ui")[0]
print(formTableOrders)
class MainWindowClass(QMainWindow, formMain):
	def __init__(self):
		QMainWindow.__init__(self, None)
		self.setupUi(self)
		self.setWindowTitle('Окно входа')
		self.gotoReg()
		self.checking()

	def gotoReg(self):
		self.signButton.clicked.connect(self.show_RegWindow)
		self.signButton.clicked.connect(self.close)

	def show_RegWindow(self):
		self.rw = RegWindowClass()
		self.rw.show()

	def checking(self):
		self.loginButton.clicked.connect(self.check)

	def getData(self):
		log = self.loginLine.text()
		pas = self.passLine.text()
		data = [log, pas]
		return data


	def check(self):
		con = sqlite3.connect('regDB.db')
		cur = con.cursor()
		cur.execute('CREATE TABLE IF NOT EXISTS vhod_data(login TEXT,'
												   	'password TEXT)')
		find_user = ('SELECT * FROM reg_data WHERE login = ? AND password = ?')
		cur.execute(find_user, self.getData())
		rows = cur.fetchall()
		print(rows)
		if rows != []:
			print("Успех")
			self.close()
			self.tw = OrdersTableClass()
			self.tw.show()
		else:
			print("Ошибка ввода")
			self.ew = ErrorClass()
			self.ew.show()

		con.commit()
		cur.close()
		con.close()

class OrdersTableClass(QMainWindow, formTableOrders):
	def __init__(self):
		QMainWindow.__init__(self, None)
		self.setupUi(self)
		self.setupUi.setItem(0, 0, QTableWidgetItem("Text in column 1"))(self)







class ErrorClass(QWidget, formError):
	def __init__(self):
		QWidget.__init__(self, None)
		self.setupUi(self)
		self.closing()

	def closing(self):
		self.okButton.clicked.connect(self.close)

class MessBoxClass(QWidget, formError):
	def __init__(self):
		QWidget.__init__(self, None)
		self.setupUi(self)
		self.label.setText("Вы успешно зарегистрированы в системе!")
		self.setWindowTitle('Успех!')
		self.closing()


	def closing(self):
		self.okButton.clicked.connect(self.close)
		


class RegWindowClass(QDialog, formDialog):
	def __init__(self):
		QWidget.__init__(self, None)
		self.setupUi(self)
		self.setWindowTitle('Окно регистрации')
		self.registration()
		self.backToMain()


	def registration(self):
		self.regButton.clicked.connect(self.ifZero)

	def showMessBox(self):
		self.mb = MessBoxClass()
		self.mb.show()

	def backToMain(self):
		self.backButton.clicked.connect(self.showMainWin)
		self.backButton.clicked.connect(self.close)

	def showMainWin(self):
		self.mw = MainWindowClass()
		self.mw.show()

	def ifZero(self):
		if len(self.nameLine.text()) == 0 or len(self.loginLine.text()) == 0 or len(self.passLine.text()) == 0:
			self.ew = ErrorClass()
			self.ew.label.setText("Заполните поля для ввода!")
			self.ew.show()
		else:
			self.get_all_lineText_to_DB()
		print(len(self.nameLine.text()))
			


	def getText(self):
		name = self.nameLine.text()
		login = self.loginLine.text()
		pas = self.passLine.text()
		data = [name, login, pas]
		return data

	def getLoginText(self):
		login = self.loginLine.text()
		data = [login]
		return data



	def get_all_lineText_to_DB(self):
		con = sqlite3.connect('regDB.db')
		cur = con.cursor()
		cur.execute('CREATE TABLE IF NOT EXISTS reg_data(Fio TEXT,'
												   'login TEXT,'
												   'password TEXT)')
		find_user = ('SELECT * FROM reg_data WHERE login = ?')
		cur.execute(find_user, self.getLoginText())
		rows = cur.fetchall()

		if rows == []:
			cur.execute('INSERT INTO reg_data VALUES(?,?,?)', self.getText())
			self.showMessBox()

		else:
			self.ew = ErrorClass()
			self.ew.label.setText("Пользователь с таким логином уже есть в системе")
			self.ew.show()
		con.commit()
		cur.close()
		con.close()



if __name__ == "__main__":
	app = QApplication(sys.argv)
	MainWindow = MainWindowClass()
	MainWindow.show()
	app.exec_()



