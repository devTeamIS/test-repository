# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Python\PAS\2.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1000, 566)
        MainWindow.setMinimumSize(QtCore.QSize(1000, 566))
        MainWindow.setMaximumSize(QtCore.QSize(1000, 566))
        MainWindow.setStyleSheet("position: absolute;\n"
"width: 1000px;\n"
"height: 566px;\n"
"left: 0px;\n"
"top: 34px;\n"
"\n"
"background: linear-gradient(180deg, rgba(255, 255, 255, 0) 0%, #FFFFFF 100%), url(povar.jpg);\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(20, 50, 399, 472))
        self.frame.setMinimumSize(QtCore.QSize(399, 472))
        self.frame.setMaximumSize(QtCore.QSize(399, 472))
        self.frame.setStyleSheet("position: absolute;\n"
"width: 399px;\n"
"height: 472px;\n"
"left: 33px;\n"
"top: 84px;\n"
"\n"
"/* Blue 2 */\n"
"\n"
"background: #2D9CDB;")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.mainLabel = QtWidgets.QLabel(self.frame)
        self.mainLabel.setGeometry(QtCore.QRect(10, 0, 391, 101))
        self.mainLabel.setStyleSheet("position: absolute;\n"
"width: 349px;\n"
"height: 56px;\n"
"left: 58px;\n"
"top: 115px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 24px;\n"
"line-height: 28px;\n"
"\n"
"color: #FFFFFF;")
        self.mainLabel.setObjectName("mainLabel")
        self.loginLabel = QtWidgets.QLabel(self.frame)
        self.loginLabel.setGeometry(QtCore.QRect(30, 130, 101, 16))
        self.loginLabel.setStyleSheet("position: absolute;\n"
"width: 52px;\n"
"height: 21px;\n"
"left: 58px;\n"
"top: 217px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"/* identical to box height */\n"
"\n"
"\n"
"color: #FFFFFF;")
        self.loginLabel.setObjectName("loginLabel")
        self.passLabel = QtWidgets.QLabel(self.frame)
        self.passLabel.setGeometry(QtCore.QRect(30, 250, 81, 20))
        self.passLabel.setStyleSheet("position: absolute;\n"
"width: 64px;\n"
"height: 21px;\n"
"left: 58px;\n"
"top: 321px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"/* identical to box height */\n"
"\n"
"\n"
"color: #FFFFFF;\n"
"")
        self.passLabel.setObjectName("passLabel")
        self.loginLine = QtWidgets.QLineEdit(self.frame)
        self.loginLine.setGeometry(QtCore.QRect(30, 160, 349, 36))
        self.loginLine.setMinimumSize(QtCore.QSize(349, 36))
        self.loginLine.setMaximumSize(QtCore.QSize(349, 36))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.loginLine.setFont(font)
        self.loginLine.setStyleSheet("position: absolute;\n"
"width: 349px;\n"
"height: 36px;\n"
"left: 58px;\n"
"top: 249px;\n"
"\n"
"background: #FFFFFF;")
        self.loginLine.setObjectName("loginLine")
        self.passLine = QtWidgets.QLineEdit(self.frame)
        self.passLine.setGeometry(QtCore.QRect(30, 280, 349, 36))
        self.passLine.setMinimumSize(QtCore.QSize(349, 36))
        self.passLine.setMaximumSize(QtCore.QSize(349, 36))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.passLine.setFont(font)
        self.passLine.setStyleSheet("position: absolute;\n"
"width: 349px;\n"
"height: 36px;\n"
"left: 58px;\n"
"top: 249px;\n"
"\n"
"background: #FFFFFF;")
        self.passLine.setObjectName("passLine")
        self.loginButton = QtWidgets.QPushButton(self.frame)
        self.loginButton.setGeometry(QtCore.QRect(30, 380, 349, 61))
        self.loginButton.setMinimumSize(QtCore.QSize(349, 61))
        self.loginButton.setMaximumSize(QtCore.QSize(349, 61))
        self.loginButton.setStyleSheet("QPushButton{\n"
"position: absolute;\n"
"width: 349px;\n"
"height: 61px;\n"
"left: 58px;\n"
"top: 457px;\n"
"border:none;\n"
"position: absolute;\n"
"width: 51px;\n"
"height: 21px;\n"
"left: 207px;\n"
"top: 478px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"/* identical to box height */\n"
"\n"
"\n"
"color: #FFFFFF;\n"
"\n"
"background: #006EAC;}\n"
"\n"
"QPushButton:hover{\n"
"background-color:#005da7;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:#2D9CDB;\n"
"}\n"
"\n"
"")
        self.loginButton.setObjectName("loginButton")
        self.backButton = QtWidgets.QPushButton(self.centralwidget)
        self.backButton.setGeometry(QtCore.QRect(710, 420, 254, 33))
        self.backButton.setMinimumSize(QtCore.QSize(254, 33))
        self.backButton.setMaximumSize(QtCore.QSize(254, 33))
        self.backButton.setStyleSheet("QPushButton{position: absolute;\n"
"width: 254px;\n"
"height: 33px;\n"
"left: 695px;\n"
"top: 495px;\n"
"border:none;\n"
"background: #2D9CDB;\n"
"position: absolute;\n"
"width: 132px;\n"
"height: 21px;\n"
"left: 756px;\n"
"top: 463px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"/* identical to box height */\n"
"\n"
"\n"
"color: #FFFFFF;}\n"
"\n"
"QPushButton:hover{\n"
"background-color:#005da7;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:#2D9CDB;\n"
"}")
        self.backButton.setObjectName("backButton")
        self.signButton = QtWidgets.QPushButton(self.centralwidget)
        self.signButton.setGeometry(QtCore.QRect(710, 460, 254, 61))
        self.signButton.setMinimumSize(QtCore.QSize(254, 61))
        self.signButton.setMaximumSize(QtCore.QSize(254, 61))
        self.signButton.setStyleSheet("QPushButton{position: absolute;\n"
"width: 254px;\n"
"height: 61px;\n"
"left: 695px;\n"
"top: 495px;\n"
"border:none;\n"
"background: #2D9CDB;\n"
"position: absolute;\n"
"width: 132px;\n"
"height: 21px;\n"
"left: 756px;\n"
"top: 463px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"/* identical to box height */\n"
"\n"
"\n"
"color: #FFFFFF;}\n"
"\n"
"QPushButton:hover{\n"
"background-color:#005da7;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:#2D9CDB;\n"
"}")
        self.signButton.setObjectName("signButton")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.mainLabel.setText(_translate("MainWindow", " Войдите в свою учётную \n"
" запись \"Рабочий\""))
        self.loginLabel.setText(_translate("MainWindow", "Логин"))
        self.passLabel.setText(_translate("MainWindow", "Пароль"))
        self.loginButton.setText(_translate("MainWindow", "Войти"))
        self.backButton.setText(_translate("MainWindow", "Вернуться назад"))
        self.signButton.setText(_translate("MainWindow", "Создать учётную запись"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
