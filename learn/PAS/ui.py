# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Python\PAS\untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(500, 600)
        Dialog.setMinimumSize(QtCore.QSize(500, 600))
        Dialog.setMaximumSize(QtCore.QSize(500, 600))
        Dialog.setStyleSheet("position: absolute;\n"
"width: 500px;\n"
"height: 566px;\n"
"left: 0px;\n"
"top: 34px;\n"
"\n"
"\n"
"background: #2D9CDB;")
        Dialog.setSizeGripEnabled(False)
        Dialog.setModal(False)
        self.nameLine = QtWidgets.QLineEdit(Dialog)
        self.nameLine.setGeometry(QtCore.QRect(20, 160, 461, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.nameLine.setFont(font)
        self.nameLine.setStyleSheet("position: absolute;\n"
"width: 436px;\n"
"height: 36px;\n"
"left: 35px;\n"
"top: 158px;\n"
"\n"
"background: #FFFFFF;")
        self.nameLine.setObjectName("nameLine")
        self.passLine = QtWidgets.QLineEdit(Dialog)
        self.passLine.setGeometry(QtCore.QRect(20, 350, 461, 41))
        self.passLine.setClearButtonEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.passLine.setFont(font)
        self.passLine.setStyleSheet("position: absolute;\n"
"width: 436px;\n"
"height: 36px;\n"
"left: 35px;\n"
"top: 158px;\n"
"\n"
"background: #FFFFFF;")
        self.passLine.setObjectName("passLine")
        self.loginLine = QtWidgets.QLineEdit(Dialog)
        self.loginLine.setGeometry(QtCore.QRect(20, 260, 461, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.loginLine.setFont(font)
        self.loginLine.setClearButtonEnabled(True)
        self.loginLine.setStyleSheet("position: absolute;\n"
"width: 436px;\n"
"height: 36px;\n"
"left: 35px;\n"
"top: 158px;\n"
"\n"
"background: #FFFFFF;")
        self.loginLine.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.loginLine.setText("")
        self.loginLine.setFrame(True)
        self.loginLine.setClearButtonEnabled(True)
        self.loginLine.setObjectName("loginLine")
        self.mainLabel = QtWidgets.QLabel(Dialog)
        self.mainLabel.setGeometry(QtCore.QRect(30, 30, 261, 71))
        self.mainLabel.setStyleSheet("position: absolute;\n"
"width: 263px;\n"
"height: 28px;\n"
"left: 30px;\n"
"top: 60px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 24px;\n"
"line-height: 28px;\n"
"color: #FFFFFF;")
        self.mainLabel.setObjectName("mainLabel")
        self.nameLabel = QtWidgets.QLabel(Dialog)
        self.nameLabel.setGeometry(QtCore.QRect(20, 130, 51, 16))
        self.nameLabel.setStyleSheet("position: absolute;\n"
"width: 53px;\n"
"height: 21px;\n"
"left: 36px;\n"
"top: 129px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"color: #FFFFFF;")
        self.nameLabel.setObjectName("nameLabel")
        self.loginLable = QtWidgets.QLabel(Dialog)
        self.loginLable.setGeometry(QtCore.QRect(20, 230, 51, 16))
        self.loginLable.setStyleSheet("position: absolute;\n"
"width: 52px;\n"
"height: 21px;\n"
"left: 38px;\n"
"top: 209px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"color: #FFFFFF;")
        self.loginLable.setObjectName("loginLable")
        self.passLabel = QtWidgets.QLabel(Dialog)
        self.passLabel.setGeometry(QtCore.QRect(20, 320, 71, 16))
        self.passLabel.setStyleSheet("position: absolute;\n"
"width: 64px;\n"
"height: 21px;\n"
"left: 38px;\n"
"top: 286px;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"color: #FFFFFF;")
        self.passLabel.setObjectName("passLabel")
        self.regButton = QtWidgets.QPushButton(Dialog)
        self.regButton.setGeometry(QtCore.QRect(20, 450, 461, 71))
        self.regButton.setStyleSheet("QPushButton{position: absolute;\n"
"width: 437px;\n"
"height: 61px;\n"
"left: 35px;\n"
"top: 466px;\n"
"border:none;\n"
"\n"
"background: #006EAC;\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"\n"
"text-align: center;\n"
"color: #FFFFFF;}\n"
"\n"
"QPushButton:hover{\n"
"background-color:#005da7;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:#2D9CDB;\n"
"}")
        self.regButton.setCheckable(False)
        self.regButton.setDefault(False)
        self.regButton.setFlat(False)
        self.regButton.setObjectName("regButton")
        self.backButton = QtWidgets.QPushButton(Dialog)
        self.backButton.setGeometry(QtCore.QRect(20, 542, 461, 31))
        self.backButton.setStyleSheet("QPushButton{position: absolute;\n"
"width: 437px;\n"
"height: 33px;\n"
"left: 35px;\n"
"top: 542px;\n"
"border:none;\n"
"\n"
"background: #006EAC;\n"
"\n"
"font-family: Roboto;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 18px;\n"
"line-height: 21px;\n"
"\n"
"text-align: center;\n"
"\n"
"color: #FFFFFF;}\n"
"QPushButton:hover{\n"
"background-color:#005da7;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:2D9CDB;\n"
"}\n"
"")
        self.backButton.setObjectName("backButton")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.mainLabel.setText(_translate("Dialog", "Регистрация \"Рабочий\""))
        self.nameLabel.setText(_translate("Dialog", "Ф.И.О"))
        self.loginLable.setText(_translate("Dialog", "Логин"))
        self.passLabel.setText(_translate("Dialog", "Пароль"))
        self.regButton.setText(_translate("Dialog", "Зарегистрироваться"))
        self.backButton.setText(_translate("Dialog", "Вернуться назад"))
