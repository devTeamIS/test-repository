from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from ui_sign import Ui_MainWindow
from ui import Ui_Dialog
import sqlite3

# Create application
app = QtWidgets.QApplication(sys.argv)

# Create form and init UI
Dialog = QtWidgets.QDialog()
ui = Ui_Dialog()
ui.setupUi(Dialog)
Dialog.show()

# Create DB
def get_all_lineText_to_DB():
	nameLine = ui.nameLine.text()
	loginLine = ui.loginLine.text()
	passLine = ui.passLine.text()
	con = sqlite3.connect('regDB.db')
	cur = con.cursor()
	cur.execute('CREATE TABLE IF NOT EXISTS reg_data(Fio TEXT,'
												   'login TEXT,'
												   'password TEXT)')
	data = [nameLine, loginLine, passLine]
	cur.execute('INSERT INTO reg_data VALUES(?,?,?)', data)
	con.commit()
	cur.close()
	con.close()

# sign in frame
def signFrame():
	pass



# Binds
ui.regButton.clicked.connect(get_all_lineText_to_DB)
ui.backButton.clicked.connect(nameLine.signFrame)

# Run mainloop
sys.exit(app.exec_())