# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Python\PAS\Orders.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1300, 650)
        MainWindow.setMinimumSize(QtCore.QSize(1300, 650))
        MainWindow.setMaximumSize(QtCore.QSize(1300, 650))
        MainWindow.setStyleSheet("background: #DBDBDB;")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(40, 110, 1101, 481))
        self.tableWidget.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.tableWidget.setFrameShadow(QtWidgets.QFrame.Plain)
        self.tableWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.tableWidget.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.tableWidget.setShowGrid(True)
        self.tableWidget.setGridStyle(QtCore.Qt.SolidLine)
        self.tableWidget.setRowCount(1)
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setObjectName("tableWidget")
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(40, 10, 1171, 71))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.ordersButton = QtWidgets.QPushButton(self.frame)
        self.ordersButton.setEnabled(True)
        self.ordersButton.setGeometry(QtCore.QRect(9, 10, 201, 51))
        self.ordersButton.setStyleSheet("QPushButton{background: #C4C4C4;\n"
"border:none;\n"
"font-family: Arial;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 16px;\n"
"line-height: 14px;\n"
"color: #006EAC;}\n"
"QPushButton:hover{\n"
"background: #818181;\n"
"}\n"
"QPushButton:pressed{\n"
"background: #6C6C6C;\n"
"}")
        self.ordersButton.setObjectName("ordersButton")
        self.myOrdersButton = QtWidgets.QPushButton(self.frame)
        self.myOrdersButton.setGeometry(QtCore.QRect(240, 10, 201, 51))
        self.myOrdersButton.setStyleSheet("QPushButton{\n"
"background: #C4C4C4;\n"
"border:none;\n"
"font-family: Arial;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 16px;\n"
"line-height: 14px;\n"
"color: #006EAC;}\n"
"QPushButton:hover{\n"
"background: #818181;\n"
"}\n"
"QPushButton:pressed{\n"
"background: #6C6C6C;\n"
"}")
        self.myOrdersButton.setObjectName("myOrdersButton")
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(1170, 109, 120, 481))
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.label = QtWidgets.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(16, 9, 91, 21))
        self.label.setStyleSheet("font-family: Arial;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 12px;\n"
"line-height: 14px;\n"
"color: #006EAC;\n"
"")
        self.label.setObjectName("label")
        self.startButton = QtWidgets.QPushButton(self.frame_2)
        self.startButton.setGeometry(QtCore.QRect(10, 50, 101, 51))
        self.startButton.setStyleSheet("QPushButton{background: #C4C4C4;\n"
"border:none;\n"
"font-family: Arial;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 16px;\n"
"line-height: 14px;\n"
"color: #006EAC;}\n"
"QPushButton:hover{\n"
"background: #818181;\n"
"}\n"
"QPushButton:pressed{\n"
"background: #6C6C6C;\n"
"}")
        self.startButton.setObjectName("startButton")
        self.infoButton = QtWidgets.QPushButton(self.frame_2)
        self.infoButton.setGeometry(QtCore.QRect(10, 110, 101, 51))
        self.infoButton.setStyleSheet("QPushButton{background: #C4C4C4;\n"
"border:none;\n"
"font-family: Arial;\n"
"font-style: normal;\n"
"font-weight: normal;\n"
"font-size: 16px;\n"
"line-height: 14px;\n"
"color: #006EAC;}\n"
"QPushButton:hover{\n"
"background: #818181;\n"
"}\n"
"QPushButton:pressed{\n"
"background: #6C6C6C;\n"
"}")
        self.infoButton.setObjectName("infoButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1300, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Orders"))
        self.tableWidget.setSortingEnabled(False)
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "№"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Наименование"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Комментарий"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Колличество"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Статус"))
        self.ordersButton.setText(_translate("MainWindow", "Заказы"))
        self.myOrdersButton.setText(_translate("MainWindow", "Мои заказы"))
        self.label.setText(_translate("MainWindow", "Действия"))
        self.startButton.setText(_translate("MainWindow", "Начать"))
        self.infoButton.setText(_translate("MainWindow", "Информация"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
