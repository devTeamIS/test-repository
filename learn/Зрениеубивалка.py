import pygame
import random

FPS=60
W = 700
H = 400
BLACK = (0, 0, 0)
BLUE = (0, 70, 225)
UP = 'to the up'
DOWN = 'to the down'
RIGHT = 'to the right'
LEFT = 'to the left'
 
pygame.init()
sc = pygame.display.set_mode((W, H))
clock = pygame.time.Clock()
 
x = W // 2
y = H // 2
r = 30
 
while 1:

    pygame.draw.circle(sc, BLUE, (x, y), r)

    pygame.display.update()
 
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            exit()
        if i.type == pygame.MOUSEBUTTONDOWN:
        	if i.button == 2:
        		sc.fill(BLACK)
        		pygame.display.update()

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        x -= 3
    elif keys[pygame.K_RIGHT]:
        x += 3

    if keys[pygame.K_UP]:
    	y -=3
    elif keys[pygame.K_DOWN]:
        y += 3

    if sc.fill(BLACK):
    	r=random.randint(0, 500)


    clock.tick(FPS)