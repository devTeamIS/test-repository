import sqlite3
import os

class Sqlibr:
	def __init__(self):
		super().__init__()
		self.con = None
		self.cursor = None
		self.nameDateBase = None
		self.pathDateBase = None

	def createFolder(self, name_db, put):
		if not(os.path.exists(put + '/DB')):
			os.chdir(put)
			os.mkdir('DB')
		return True

	def createDB(self, name_db):
		self.con = sqlite3.connect('./DB/' + name_db + '.db')
		self.cursor = self.con.cursor()

	def creatingTable(self, name_table):
		try:
			self.cursor.execute('CREATE TABLE ' + name_table + '(id INTEGER PRIMARY KEY AUTOINCREMENT)')
		except sqlite3.OperationalError:
			pass

	def addingColumn(self, name_table, *arr):
		arr = list(arr[i].split(':') for i in range(len(arr)))
		for i in range(len(arr)):
			try:
				self.cursor.execute('ALTER TABLE ' + name_table + ' ADD ' + arr[i][0] + ' ' + arr[i][1])
			except sqlite3.OperationalError:
				pass

	def addintable(self, name_table):
		# flo = str(input('Enter FIO: '))
		# loin = str(input('Enter login: '))
		# passord = str(input('Enter password: '))
		# data = [flo, loin, passord]
		# self.cursor.execute('INSERT INTO ' + name_table + ' VALUES(NULL,?,?,?)', data)
		big_data=[]
		data1=['gre', 'jeck', '123']
		data2=['ripp', 'jeck', '423']
		big_data.append(data1)
		big_data.append(data2)
		for data_unit in big_data:
			self.cursor.execute('INSERT INTO ' + name_table + ' VALUES(NULL,?,?,?)', data_unit)
		self.con.commit()
		self.cursor.close()
		self.con.close()

	# РЕДАКТИРОВАНИЕ работает. Меняет одни ники на другие
	def redaktirobnov(self, name_table, nik, ner, nid, pers):
		self.cursor.execute('UPDATE ' + name_table + ' SET ' + nik + " =  '" + ner+"'"' WHERE ' + nid + " =  '" + pers + "'")
		self.con.commit()

	# УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ работает
	def delet(self, name_table, ide, record_id): 
		torn = 'DELETE FROM ' + name_table + ' WHERE ' + ide + " =  '" + record_id + "'"
		self.cursor.execute(torn)
		self.con.commit()